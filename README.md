 ![ |small ](imgs/title.png)
 
Material for Intro to Programming in Python. The material intends to expose new students to basic programming concepts using the python
language to better prepare them for their own research interests that will make use of some programming. 


## Topics:
* Day 1 Topics:
  * [Jupyter Lab IDE & Notebook Basics](notebooks/jupyter_intro.ipynb)
  * Data Structures in Python
  * Manipulating Lists and Arrays
  * Loops and Control Flow
 
* Day 2 Topics:
  * [Functions](notebooks/functions.ipynb)
  * File I/O
  * Plotting and Visualization 

* Day 3 Topics:
  * Structuring a Program
  * More Examples With numpy
  * Other Scientific Libraries

## Running the Collection of Tutorials
[![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gl/jhskone%2Fintro-to-python-programming/HEAD?filepath=notebooks/master.ipynb)

There are no installation requirements for your local computer in order to view 
and run any of the ipython notebooks used in this short course. Instead we will 
rely on cloud compute resources that you will connect to through your web browser.
In order to access an interactive environment to view and execute the python 
programming jupyter notebooks, you will need to click the *binder* link at the top
left of this section. This will open up a new browser window tab that will provision 
a lightweight remote cloud resource that will spin up a jupyter notebook server 
for you to interactively work with the notebooks and other content of this repo.
Please have patience as the process of cloning the repo, creating a docker image,
installing all python requirements, and launching jupyter lab, can take
a minute or more to complete. 

In the event there are technical difficulties with using mybinder, we can use
[Google's Colaboratory](https://colab.research.google.com) as a fall back. The 
Colaboratory environment is a slight modification of the jupyter notebook server, 
but essentially does the same thing. If we need to resort to using Colaboratory,
we will have to import this repository and unfortunately Colaboratory only 
supports GitHub at this time, so we will have to use a mirror of this repo. 

## Using Python on Your Personal Computer

Most of your personal computers will come with python 2.7 already installed, but 
to take full advantage of newer features and keep yourself contemporary, you'll 
need to upgrade yourself to at least version 3.5 of python. The end-of-life for 
the 3.5 version of python (i.e. the time at which no further updates or patches 
will be released is 09,13,2020). For reference the newest stable release 3.x version
of python (3.7) has an end-of-life scheduled for 06,27,2023.

### Installing Anaconda Python
**Note:** *You do not need to install anything to view and execute the examples 
for this three day workshop. The installation instructions provided here are for 
you to be able to run python on your own personal resource if you wish to in the 
future.*

There are several ways in which one can go about installing or upgrading python
on their system. The [python software foundation](https://www.python.org/) is responsible for managing and 
distributing various releases of python. You could directly download from their 
site a version of python for your particular OS, but this is is not the recommended
way of upgrading yourself. You could also use a package manager if using linux 
(yum, apt-get, etc.) or a mac (home brew, mac ports, etc.) to system install 
python, but again this is not the easiest solution, requires root access on the
machine, and furthermore can complicate your managment of python on your system. 

The simplest and recommended solution is for you to install the Anaconda 
distribution of python. <br/>
 ![ |small ](imgs/anaconda.png)

Why Anaconda?...<br/>
The Anaconda distribution of python comes prepackaged with most of the python 
packages you would want to use for scientific computing, like numpy, matplotlib, 
scipy, and jupyter notebook server/lab. Furthermore it comes packaged with 
Intel's MKL math libraries that the numpy and scipy functions link against, 
providing you with faster linear algebra under the hood without any additional 
effort on your part to configure this. 

Follow the [Anaconda Python Installation Instructions](https://docs.anaconda.com/anaconda/install/)
provided on the Continuum Analytics site for your particular operating system. 
Download a 3.x version (either the latest or a version no older than 3.5). The 
miniconda version of Anaconda provides a bare bones install of python with the 
conda package manager. It then requires you to install all of the packages you 
require. It is therefore not recommended that you install the miniconda version
as this does not come out of the box with any of the scientific packages preinstalled. 

